package com.cd;

public class Solution {

	public static void main(String[] args) {
		String INPUT = "{}}{}{{}{}";
		INPUT = removeLettersAndDigits(INPUT);
		INPUT = removeTwoSuccessiveReverseCharacters(INPUT);
		if ("".equals(INPUT)) {
			System.out.println(true);
		} else {
			System.out.println(false);
		}
	}

	static String removeLettersAndDigits(String INPUT) {
		String result = "";
		for (int i = 0; i < INPUT.length(); i++) {
			if (INPUT.charAt(i) == '{' || INPUT.charAt(i) == '}' || INPUT.charAt(i) == '(' || INPUT.charAt(i) == ')'
					|| INPUT.charAt(i) == '[' || INPUT.charAt(i) == ']') {
				result = result + INPUT.charAt(i);
			}
		}
		return result;
	}

	static String removeTwoSuccessiveReverseCharacters(String INPUT) {
		StringBuilder in = new StringBuilder(INPUT);
		int i = 0, j = 1;
		while (j <= in.length()) {
			switch (in.charAt(i)) {
			case '{':
				if (in.charAt(j) == '}') {
					in = in.delete(i, j + 1);
					i = -1;
					j = 0;
				}
				break;
			case '(':
				if (in.charAt(j) == ')') {
					in = in.delete(i, j + 1);
					i = -1;
					j = 0;
				}
				break;
			case '[':
				if (in.charAt(j) == ']') {
					in = in.delete(i, j + 1);
					i = -1;
					j = 0;
				}
				break;
			default:
				return in.toString();
			}
			j++;
			i++;
			if (in.length() == 1) {
				return in.toString();
			}
		}
		return in.toString();
	}
}
